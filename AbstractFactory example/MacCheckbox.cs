﻿namespace AbstractFactory_example
{
    using AbstractFactory_example.Interfaces;

    public class MacCheckbox : ICheckbox
    {
        public MacCheckbox() { }

        public void Paint()
        {
            Console.WriteLine("I am a MacCheckBox");
        }
    }
}
