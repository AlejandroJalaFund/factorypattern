﻿using AbstractFactory_example;

WinFactory winFactory = new WinFactory();
MacFactory macFactory = new MacFactory();

Application app = new Application(winFactory);

app.CreateUIButton();
app.CreateUICheckbox();

app.PaintButton();
app.PaintCheckBox();
