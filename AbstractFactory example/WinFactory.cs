﻿namespace AbstractFactory_example
{
    using AbstractFactory_example.Interfaces;

    public class WinFactory : IGUIFactory
    {
        public WinFactory() { }

        public IButton CreateButton()
        {
            return new WinButton();
        }

        public ICheckbox CreateChechbox()
        {
            return new WinCheckbox();
        }
    }
}
