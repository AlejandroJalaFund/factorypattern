﻿namespace AbstractFactory_example
{
    using AbstractFactory_example.Interfaces;

    public class MacButton : IButton
    {
        public MacButton() { }

        public void Paint()
        {
            Console.WriteLine("I am a MacButton");
        }
    }
}
