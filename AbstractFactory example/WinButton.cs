﻿namespace AbstractFactory_example
{
    using AbstractFactory_example.Interfaces;

    public class WinButton : IButton
    {
        public WinButton() { }

        public void Paint()
        {
            Console.WriteLine("I am a WinButton");
        }
    }
}
