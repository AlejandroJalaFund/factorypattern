﻿namespace AbstractFactory_example
{
    using AbstractFactory_example.Interfaces;

    internal class WinCheckbox : ICheckbox
    {
        public WinCheckbox() { }

        public void Paint()
        {
            Console.WriteLine("I am a WinCheckbox");
        }
    }
}
