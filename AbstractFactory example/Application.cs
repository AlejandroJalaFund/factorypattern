﻿namespace AbstractFactory_example
{
    using AbstractFactory_example.Interfaces;

    public class Application
    {
        private IGUIFactory guIFactory;
        private IButton button;
        private ICheckbox checkbox;

        public Application(IGUIFactory concreteFactory)
        {
            this.guIFactory = concreteFactory;
        }

        public void CreateUIButton()
        {
            this.button = this.guIFactory.CreateButton();
        }

        public void CreateUICheckbox()
        {
            this.checkbox = this.guIFactory.CreateChechbox();
        }

        public void PaintButton()
        {
            this.button.Paint();
        }

        public void PaintCheckBox()
        {
            this.checkbox.Paint();
        }
    }
}
