﻿namespace AbstractFactory_example.Interfaces
{
    public interface ICheckbox
    {
        void Paint();
    }
}
