﻿namespace AbstractFactory_example.Interfaces
{
    public interface IButton
    {
        void Paint();
    }
}
