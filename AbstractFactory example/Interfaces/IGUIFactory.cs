﻿namespace AbstractFactory_example.Interfaces
{
    // grafical user interface
    public interface IGUIFactory
    {
        IButton CreateButton();

        ICheckbox CreateChechbox();
    }
}
