﻿namespace AbstractFactory_example
{
    using AbstractFactory_example.Interfaces;

    public class MacFactory : IGUIFactory
    {
        public MacFactory() { }

        public IButton CreateButton()
        {
            return new MacButton();
        }

        public ICheckbox CreateChechbox()
        {
            return new MacCheckbox();
        }
    }
}
